# Leaf:
#     Data
#         X
#         y
#     TreeNode
    
# TreeNode:
#     LChild
#     RChild
#     Split
#         Feature
#         Value
#     Prediction

import numpy as np
import math
import bz2

Data = None
T    = None    # Root of the Tree


def forecast(y):
    
    yl  = list(y)
    val = max(set(y), key=yl.count)
    
    return val


def predict(node, Xi):
    
    if node['Split']['Feature'] == None:
        return node['Prediction']
    
    index = node['Split']['Feature']
    value = node['Split']['Value']

    if Xi[index] <= value:
        if node['LChild'] != None:
            y = predict(node['LChild'], Xi)
        else:
            return node['Prediction']
    else:
        if node['RChild'] != None:
            y = predict(node['RChild'], Xi)
        else:
            return node['Prediction']

    return y


def cost_function():

    red = redundancy()
    ina = inaccuracy()
    
    if red < ina:
        red = 1
    
    cost = 2 / ( (1/red) + (1/ina) )            

    print("Redundancy: " + str(red) + " Inaccuracy: " + str(ina) + " Cost: " + str(cost))
    
    return cost


def redundancy():
    
    model      = tree2str().encode()
    compressed = bz2.compress(model, compresslevel=9)
    redundancy = 1 - len(compressed) / len(model)
    
    return redundancy


def head2str(node):

    myset = set()
    
    if node['Split']['Feature'] == None:
        return myset

    myset.add('X%d' % (node['Split']['Feature']+1))

    if node['LChild'] != None:
        myset = myset.union(head2str(node['LChild']))
    
    if node['RChild'] != None:
        myset = myset.union(head2str(node['RChild']))
                
    return myset


def body2str(node, depth):

    string = ""
    
    if node['Split']['Feature'] == None:
        string = string + '%sreturn %s\n' % (' '*(depth+1)*4, node['Prediction'])
        return string
    
    string = string + '%sif X%d <= %.3f:\n' % (' '*depth*4, (node['Split']['Feature']+1), node['Split']['Value'])

    if node['LChild'] == None:
        string = string + '%sreturn %s\n' % (' '*(depth+1)*4, node['Prediction'])
    else:
        string = string + body2str(node['LChild'],  depth+1)
    
    string = string + '%selse:\n' % (' '*depth*4)
    
    if node['RChild'] == None:
        string = string + '%sreturn %s\n' % (' '*(depth+1)*4, node['Prediction'])
    else:
        string = string + body2str(node['RChild'], depth+1)
                
        return string

    
def tree2str():
    
    global T
    
    string = "def tree" + str(head2str(T)) + ":\n"
    string = string + body2str(T, 1)

    return string


def inaccuracy():
    
    global T, X, y
    
    error = list()
    for i in range(len(X)):
        pred = predict(T, X[i])
        if pred != y[i]:
            error.append(list(X[i]))

    error  = str(error).encode()
    dmodel = bz2.compress(error, compresslevel=9)        
    ldm    = len(dmodel)
        
    data  = (str(X.tolist()) + str(y.tolist())).encode()
    data  = bz2.compress(data, compresslevel=9)
    ld    = len(data)
                
    inaccuracy = ldm / ld
        
    return inaccuracy


def best_split(X, y):
        
    b_feature  = None
    b_value    = None
    b_entropy  = len(np.unique(y))
        
    for feature in range(0, len(X[0])):

        values = list(X[:,feature])
        values.sort()
                
        for i in range(1, len(values)):
                                        
            entropy = entropy_split(feature, values[i], X, y)
                                                
            if entropy < b_entropy:
                                                        
                middle = (values[i-1] + values[i]) / 2
                    
                b_feature  = feature
                b_value    = middle
                b_entropy  = entropy

    if b_feature == None or b_value == None:
        return None
                    
    return {'Feature':b_feature, 'Value':b_value}
 

def entropy_split(feature, value, X, y):
        
    ldata, rdata = split(feature, value, X, y)
                    
    length_l = len(ldata['X'])
    length_r = len(rdata['X'])
    length_t = length_l + length_r
    
    entropy_l = entropy(ldata['y'])
    entropy_r = entropy(rdata['y'])
    
    ent = (length_l / length_t) * entropy_l + (length_r / length_t) * entropy_r
            
    return ent


def split(feature, value, X, y):
        
    ldata = {'X': X[X[:,feature] < value],
             'y': y[X[:,feature] < value]}
    
    rdata = {'X': X[X[:,feature] >= value],
             'y': y[X[:,feature] >= value]}
                
    return ldata, rdata    


def entropy(y):
        
    ent = 0
    length  = len(y)
        
    if length == 0:    # Avoid dividing by 0
        return 0
        
    unique, counts = np.unique(y, return_counts=True)
            
    for i in range(len(unique)):
        
        probability = counts[i] / length
        information = 0    
            
        if probability != 0:    # Avoid computing the log2 of 0
            information = - probability * math.log2(probability)

        ent = ent + information

    return ent

    
def fit():
    
    global T, X, y
    
    # Create a leaf
    l = {'Data'    : {'X': None,
                      'y': None },
         'TreeNode': None}
    
    # l.data <- data
    l['Data']['X'] = X
    l['Data']['y'] = y
    
    # Create TreeNode r
    # r.LChild <- None
    # r.RChild <- None
    # r.Split  <- None
    r = {'LChild'    : None,
         'RChild'    : None,
         'Split'     : {'Feature' : None,
                        'Value':    None },
         'Prediction': None}
        
    # r.Prediction <- forecast(l.Data)
    r['Prediction'] = forecast(l['Data']['y'])
    
    # Create Tree T with root node r
    T = r
    
    # l.TreeNode <- r
    l['TreeNode'] = r

    # leavesSet <- {l}
    leavesList = list()
    leavesList.append(l)
    
    # bestCost <- costFunction(T)
    bestCost = cost_function()
    
    print("")
    
    # while leavesSet != 0 do:
    while len(leavesList) != 0:
        
        print("Current tree:")
        print(tree2str())
        tmp = cost_function()
        print("")

        # bestLeaf  <- None
        # bestSplit <- None
        # bestTNode <- None

        bestLeaf  = None
        bestSplit = None
        bestTNode = None
        
        # foreach l in leavesSet do:
        for i in range(len(leavesList)):
            
            l = leavesList[i]
                                    
            # Tita <- BestSplit(l.Data)
            Tita = best_split(l['Data']['X'], l['Data']['y'])
            
            # if Tita == Stop:
            if Tita == None:
                
                # LeavesSet <- LeavesSet \ {l}
                # leavesList.remove(l)
                continue
                
            else:

                # (dataL, dataR) = split(Tita, l.Data)
                                
                dataL, dataR = split(Tita['Feature'], Tita['Value'], l['Data']['X'], l['Data']['y'])
                        
                # If all the data in one side, do not create the node
                if len(dataL['X']) == 0 or len(dataR['X']) == 0:
                    continue
                
                # create tree nodes aux, auxL, auxR
                aux = {'LChild'    : None,
                       'RChild'    : None,
                       'Split'     : {'Feature' : None,
                                      'Value':    None },
                       'Prediction': None}
                
                auxL = {'LChild'    : None,
                        'RChild'    : None,
                        'Split'     : {'Feature' : None,
                                      'Value':    None },
                        'Prediction': None}

                auxR = {'LChild'    : None,
                        'RChild'    : None,
                        'Split'     : {'Feature' : None,
                                      'Value':    None },
                       'Prediction': None}                

                tmp = {'LChild'    : None,
                       'RChild'    : None,
                       'Split'     : {'Feature' : None,
                                      'Value':    None },
                       'Prediction': None}                
                
                # aux.Split <- Tita
                # aux.Prediction <- None
                # aux.LChild <- auxL
                # aux.RChild <- auxR

                aux['Split']      = Tita
                aux['Prediction'] = None
                aux['LChild']     = auxL
                aux['RChild']     = auxR
            
                # auxL.(LChild, RChild, Split) <- None                
                # auxR.(LChild, RChild, Split) <- None
            
                # (auxL.Prediction, auxR.Prediction) <- (forecast(dataL), forecast(dataR)
                auxL['Prediction'] = forecast(dataL['y'])
                auxR['Prediction'] = forecast(dataR['y'])
                        
                # replace in T node l.treeNode with aux
                tmp['LChild']     = l['TreeNode']['LChild']
                tmp['RChild']     = l['TreeNode']['RChild']
                tmp['Split']      = l['TreeNode']['Split']
                tmp['Prediction'] = l['TreeNode']['Prediction']

                l['TreeNode']['LChild']     = aux['LChild']
                l['TreeNode']['RChild']     = aux['RChild']
                l['TreeNode']['Split']      = aux['Split']
                l['TreeNode']['Prediction'] = aux['Prediction']
                
                print("Candidate tree:")
                print(tree2str())
                # C <- costFunct(T)
                C = cost_function()
                print("")
            
                # if c < bestCost then
                if C < bestCost:
                    
                    bestCost = C
                
                    # bestLeaf <- l
                    bestLeaf = i
                    
                    # bestSplit <- (dataL, dataR)
                    bestSplit = (dataL, dataR)
                    
                    # bestTNode <- aux
                    bestTNode = aux
                
                # replace in T node aux with l.treeNode
                l['TreeNode']['LChild']     = tmp['LChild']
                l['TreeNode']['RChild']     = tmp['RChild']
                l['TreeNode']['Split']      = tmp['Split']
                l['TreeNode']['Prediction'] = tmp['Prediction']
            
            
        # if bestLeaf != None then
        if bestLeaf != None:
            
            print("Approved!")
        
            # creates leaves lL, lR
            lL = {'Data': {'X': None,
                           'y': None },
                  'TreeNode': None}

            lR = {'Data': {'X': None,
                           'y': None },
                  'TreeNode': None}
            
            # lL.Data <- bestSplit.dataL
            # lL.treeNode <- bestNode.LChild
            lL['Data']     = bestSplit[0]
            lL['TreeNode'] = bestTNode['LChild']
            
            # lR.Data <- besSplit.dataR
            # lR.treeNode <- bestNode.RChild
            lR['Data']     = bestSplit[1]
            lR['TreeNode'] = bestTNode['RChild']
                    
            # replace in T node bestLeaf.treeNode with bestNode
            # bestLeaf['TreeNode'] = bestTNode
            
            leavesList[bestLeaf]['TreeNode']['LChild']     = bestTNode['LChild']
            leavesList[bestLeaf]['TreeNode']['RChild']     = bestTNode['RChild']
            leavesList[bestLeaf]['TreeNode']['Split']      = bestTNode['Split']
            leavesList[bestLeaf]['TreeNode']['Prediction'] = bestTNode['Prediction']
        
            # leavesSet <- leavesSet \ {bestLeaf} Union {lL, lR}
            leavesList.pop(bestLeaf)
            leavesList.append(lL)
            leavesList.append(lR)
        
        else:
              
            # return(T)
            return(T)

    # end While

    return(T)

import time

from sklearn.datasets import load_breast_cancer

data = load_breast_cancer()

X = data.data
y = data.target

T = fit()

print("Final tree:")
print(tree2str())