# MNPDT

Source code of the decision tree algorithm described in the paper "A Novel Hyperparameter-free Approach to Decision Tree Construction that
Avoids Overfitting by Design" (https://arxiv.org/abs/1906.01246). The code contains an example of how to use the algorithm in practice.